﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	Rigidbody2D playerRigidbody;
	public float speed = 3f;
	public float jumpspeed = 5f;
	private bool isGrounded;
	float targetmovespeed;
	public LayerMask groundLayers;
	private bool facingRight;

	void Awake () {
		playerRigidbody = GetComponent <Rigidbody2D> ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		isGrounded = Physics2D.OverlapArea(new Vector2(transform.position.x - 1f, transform.position.y - 1f),
			new Vector2(transform.position.x + 1f, transform.position.y - 1.01f), groundLayers);

		targetmovespeed = Mathf.Lerp (playerRigidbody.velocity.x, Input.GetAxisRaw ("Horizontal") * speed * Time.deltaTime, Time.deltaTime * 10);
		playerRigidbody.velocity = new Vector2 (targetmovespeed, playerRigidbody.velocity.y);

		if (Input.GetKey(KeyCode.A)) {
			transform.position += Vector3.left * speed * Time.deltaTime;
			facingRight = false;
			Facing ();
		}
		if (Input.GetKey(KeyCode.D)) {
			transform.position += Vector3.right * speed * Time.deltaTime;
			facingRight = true;
			Facing ();
		}

		if (Input.GetKeyDown (KeyCode.W) && isGrounded == true) {
			playerRigidbody.AddForce (Vector2.up * jumpspeed, ForceMode2D.Impulse);
		}
	}

	void Facing () {
		if (facingRight == false) {
			transform.localRotation = Quaternion.Euler(0, 0, 0);
		} else {
			transform.localRotation = Quaternion.Euler(0, 180, 0);
		}
	}
}
