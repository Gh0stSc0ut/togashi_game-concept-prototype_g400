﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleHook : MonoBehaviour
{

    DistanceJoint2D joint;
    Vector3 targetPos;
    RaycastHit2D hit;
    public float distance = 10f;
    public LayerMask mask;
    public LineRenderer grapple;

    public float grappleSpeed = 0.2f;
    bool isGrapple = false;

    // Use this for initialization
    void Start()
    {
        joint = GetComponent<DistanceJoint2D>();
        joint.enabled = false;
        grapple.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Q))
        {
            targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            targetPos.z = 0;

            hit = Physics2D.Raycast(transform.position, targetPos - transform.position, distance, mask);

            if (hit.collider != null)
            {
                joint.enabled = true;

                joint.connectedBody = hit.collider.gameObject.GetComponent<Rigidbody2D>();

                float connectX = hit.collider.transform.position.x - hit.point.x;
                float connectY = hit.collider.transform.position.y + hit.point.y;
                joint.connectedAnchor = new Vector2(connectX, connectY);
                joint.distance = Vector2.Distance(transform.position, hit.point);

                grapple.enabled = true;
				grapple.SetPosition (0, transform.position);
				grapple.SetPosition (1, hit.point);
            }
        }

        if (Input.GetKey(KeyCode.E))
        {
            grapple.SetPosition(0, transform.position);
            isGrapple = true;
            Retract();
        }

        if (Input.GetKey(KeyCode.Q))
        {
            grapple.SetPosition(0, transform.position);
        }

        if (Input.GetKeyUp(KeyCode.E) || Input.GetKeyUp(KeyCode.Q))
        {
            joint.enabled = false;
            isGrapple = false;
            grapple.enabled = false;
        }
    }

    void Retract()
    {
        if (isGrapple == true)
        {
            if (joint.distance < 0)
            {
                joint.distance = 0;
            }
            joint.distance -= grappleSpeed;
        }
    }
}
